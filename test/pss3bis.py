# -*- coding: latin-1 -*-
import numpy as np
from numba import jit
 
nncandidats = 30
nnformations = 10
nnvoeux = 4
ndiversite = 0.0
nsurplus = 1.2
nntours = 2
 
## @jit
def parcoursup(ncandidats,nformations,nvoeux,diversite,surplus,ntours):
    # on assigne les niveaux "intrinsèques" aux candidats (moins = mieux)
    niveaux = np.random.rand(ncandidats)
    print(niveaux)
    # les candidats formulent leurs voeux en ordre de préférence
    voeux = np.floor(np.random.rand(ncandidats,nvoeux)*nformations)
    print(voeux)
    # les formations ont un nombre de places
    places = np.random.rand(nformations)
    places = np.floor((places/(np.sum(places)))*ncandidats*surplus)+1
    classement = np.zeros((ncandidats,nformations),dtype=np.uint32)
    # les procedures de classification modifient les niveaux des candidats
    deltaniveaux = np.random.rand(ncandidats,nformations)*diversite
    print(deltaniveaux)
    # on calcule les classements des candidats par formation
    for formation in range(0,nformations):
        nivcand = niveaux + deltaniveaux[:,formation]
        classement[:,formation] = nivcand.argsort().argsort()
    print(classement)
    # dans proposition, on met les "oui"
    # dans voeuxactif, on met les acceptations des candidats
    # on commence par accepter tous les formations dans les voeux
    voeuxactif = np.zeros((ncandidats,nvoeux),dtype=np.uint32) + 1
    # dans voeuxformation, on prend le nombre de voeux pour chaque formation
    # dans classementlocal, on garde seulement les candidats ayant exprime
    # le voeu pour la formation
    # numerovoeux donne le numero du voeux d'une formation qu'un candidat a demande 
    voeuxlocal = np.zeros(nformations,dtype=np.uint32)
    classementlocal = np.zeros((ncandidats,nformations),dtype=np.uint32)
    numerovoeux = np.zeros((ncandidats,nformations),dtype=np.uint32)
    for formation in range(0,nformations):
        # pour chaque formation, on regarde les candidats dans l'ordre de son classement
        for indexcandidat in range(0,ncandidats):
            candidat = classement[indexcandidat,formation]
            # on regarde si le candidat en question a fait un voeu pour la formation
            for voeu in range(0,nvoeux):
                if (formation == voeux[candidat,voeu]):
                    # si c'est le cas, on ajoute ce candidat au classement local
                    classementlocal[voeuxlocal[formation],formation] = candidat
                    numerovoeux[voeuxlocal[formation],formation] = voeu
                    voeuxlocal[formation] = voeuxlocal[formation] + 1
    print(classementlocal)
    print(voeuxlocal)
    print(numerovoeux)
    nrefus = np.zeros(ntours,dtype=np.uint32)
    for tours in range(0,ntours):
        # les formations donnent des oui aux candidats en fonction de leur
        # classement et leur nombre de places si les candidats sont intéressés
        proposition = np.zeros((ncandidats,nvoeux),dtype=np.uint32)
        for formation in range(0,nformations):
            place = 0
            indexcandidat = 0
            while (place < places[formation] and indexcandidat < voeuxlocal[formation]):
                # tant qu'il y a des places disponibles et des candidats dans la liste
                candidat = classementlocal[indexcandidat,formation]
                # on boucle sur les candidats dans l'ordre du classement
                if (voeuxactif[candidat,numerovoeux[indexcandidat,formation]] == 1):
                    # si le candidat est intéressé, on lui propose une place
                    proposition[candidat,numerovoeux[indexcandidat,formation]] = 1
                    place = place + 1
                indexcandidat = indexcandidat + 1
        # les candidats repondent en fonction de leur preference
        # ils libèrent donc des places en mettant les voeux qui ne les
        # interessent plus en non-actif
        print(proposition)
        for candidat in range(0,ncandidats):
            mieux = 0
            for voeu in range(0,nvoeux):
                if (mieux == 1):
                    #if (mieux == 1 and proposition[candidat,voeu] == 1):
                    voeuxactif[candidat,voeu] = 0
                if (proposition[candidat,voeu] == 1):
                    mieux = 1
        # print(voeuxactif)
        pasdeplace = 0
        for candidat in range(0,ncandidats):
            if (sum(proposition[candidat]) == 0):
                pasdeplace = pasdeplace + 1
        nrefus[tours] = pasdeplace
        # print(sum(proposition.transpose()))
    return np.float32(nrefus)/np.float32(ncandidats)
         
dodo = parcoursup(nncandidats,nnformations,nnvoeux,ndiversite,nsurplus,nntours)
print(dodo)
print(dodo[0:-1]/dodo[1::])


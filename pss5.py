# -*- coding: latin-1 -*-
import numpy as np
from numba import jit
 
ncandidats = 80000
nformations = 1300
nvoeux = 8
diversite = 0.00004
surplus = 1.2
ntours = 20
 
@jit 
def gen_donnees(ncandidats,nvoeux,diversite,surplus,nformations):
    # on génère un système de formations et de candidats avec des voeux et 
    # un classement
    # il y a un nombre de candidats, chaque candidat fait un nombre de voeux
    # il y a une fraction de diversité dans le niveau de chaque candidat selon
    # les formations
    # il y a un facteur surplus de places disponible dans les formations
    # il y a un nombre de formations
    #
    # on retourne 5 listes:
    #  - la liste des voeux des candidats
    #  - la liste du nombre de places des formations
    #  - la liste du nombre de voeux exprimé pour une formation
    #  - les classements des candidats par formation (limité au nombre de voeux exprimés)
    #  - dans ce classement, l'ordre du voeu que le candidat X a émis pour la formation
    #
    # on assigne les niveaux "intrinsèques" aux candidats (moins = mieux)
    niveaux = np.random.rand(ncandidats)
    # les candidats formulent leurs voeux en ordre de leur préférence
    voeux = np.floor(np.random.rand(ncandidats,nvoeux)*nformations)
    # attention, bug: nous n'éliminons, pour l'instant, pas des doublons de voeux
    # 
    # les formations ont un nombre de places, dont la somme est à peu près le nombre de 
    # candidats multiplié par le facteur de surplus
    places = np.random.rand(nformations)
    places = np.floor((places/(np.sum(places)))*ncandidats*surplus)+1
    classement = np.zeros((ncandidats,nformations),dtype=np.uint32)
    # les procedures de classification modifient les niveaux des candidats pour chaque formation
    # avec un effect de la taille de la diversité
    deltaniveaux = np.random.rand(ncandidats,nformations)*diversite
    # on calcule les classements des candidats par formation 
    # d'abord, on le fait pour tous les candidats, qu'ils aient formulé un voeu ou non
    for formation in range(0,nformations):
        nivcand = niveaux + deltaniveaux[:,formation]
        classement[:,formation] = nivcand.argsort()
    # en suite, nous allons extraire les classements pour les candidats ayant exprimé
    # un voeu seulement
    # dans voeuxformation, on prend le nombre de voeux pour chaque formation
    # dans classementlocal, on garde seulement les candidats ayant exprime
    # le voeu pour la formation
    # numerovoeux donne le numero du voeux d'une formation qu'un candidat a demandé
    voeuxlocal = np.zeros(nformations,dtype=np.uint32)
    classementlocal = np.zeros((ncandidats,nformations),dtype=np.uint32)
    numerovoeux = np.zeros((ncandidats,nformations),dtype=np.uint32)
    for formation in range(0,nformations):
        # pour chaque formation, on regarde les candidats dans l'ordre de leur classement
        for indexcandidat in range(0,ncandidats):
            candidat = classement[indexcandidat,formation]
            # on regarde si le candidat en question a fait un voeu pour la formation
            for voeu in range(0,nvoeux):
                if (formation == voeux[candidat,voeu]):
                    # si c'est le cas, on ajoute ce candidat au classement local
                    classementlocal[voeuxlocal[formation],formation] = candidat
                    numerovoeux[voeuxlocal[formation],formation] = voeu
                    voeuxlocal[formation] = voeuxlocal[formation] + 1
    return (voeux,places,voeuxlocal,classementlocal,numerovoeux)

@jit
def reponses_formations(nformations,places,voeuxlocal,classementlocal,voeuxactif,numerovoeux):
    # les formations donnent des oui aux candidats en fonction de leur
    # classement et leur nombre de places si les candidats sont intéressés
    # dans proposition
    proposition = np.zeros((ncandidats,nvoeux),dtype=np.uint32)
    for formation in range(0,nformations):
        place = 0
        indexcandidat = 0
        while (place < places[formation] and indexcandidat < voeuxlocal[formation]):
            # tant qu'il y a des places disponibles et des candidats dans la liste
            candidat = classementlocal[indexcandidat,formation]
            # on boucle sur les candidats dans l'ordre du classement
            if (voeuxactif[candidat,numerovoeux[indexcandidat,formation]] == 1):
                # si le candidat est intéressé, on lui propose une place
                proposition[candidat,numerovoeux[indexcandidat,formation]] = 1
                place = place + 1
            indexcandidat = indexcandidat + 1
    return proposition
    
@jit
def reponses_candidat(ncandidats,nvoeux,voeuxactif,proposition):
    # les candidats repondent en fonction de leur preference
    # ils libèrent donc des places en mettant les voeux qui ne les
    # interessent plus en non-actif
    # dans cette version de réponse, les candidats gardent tous leurs
    # voeux en attente (même les voeux "plus bas" que les voeux ayant un oui)
    for candidat in range(0,ncandidats):
        mieux = 0
        for voeu in range(0,nvoeux):
            if (mieux == 1 and proposition[candidat,voeu] == 1):
                voeuxactif[candidat,voeu] = 0
            if (proposition[candidat,voeu] == 1):
                mieux = 1
    return voeuxactif
    
@jit
def reponses_candidat2(ncandidats,nvoeux,voeuxactif,proposition):
    # les candidats repondent en fonction de leur preference
    # ils libèrent donc des places en mettant les voeux qui ne les
    # interessent plus en non-actif
    # dans cette version de réponse, les candidats éliminent aussi les voeux
    # en attente qui sont classés plus bas que leur plus haut "oui".
    for candidat in range(0,ncandidats):
        mieux = 0
        for voeu in range(0,nvoeux):
            if (mieux == 1):
                voeuxactif[candidat,voeu] = 0
            if (proposition[candidat,voeu] == 1):
                mieux = 1
    return voeuxactif    

####################################################

# programme principal

# generation d'un jeu de données
voeux,places,voeuxlocal,classementlocal,numerovoeux = gen_donnees(ncandidats,nvoeux,diversite,surplus,nformations)


# dans voeuxactif, on met les acceptations des candidats
# on commence par accepter tous les formations dans les voeux
# nous allons suivre deux calculs: ceux avec la réponse type 1 
# et ceux avec la réponse type 2
voeuxactif = np.zeros((ncandidats,nvoeux),dtype=np.uint32) + 1
voeuxactif2 = np.zeros((ncandidats,nvoeux),dtype=np.uint32) + 1
    
nrefus = np.zeros(ntours,dtype=np.uint32)
nrefus2 = np.zeros(ntours,dtype=np.uint32)
for tours in range(0,ntours):
    proposition = reponses_formations(nformations,places,voeuxlocal,classementlocal,voeuxactif,numerovoeux)
    proposition2 = reponses_formations(nformations,places,voeuxlocal,classementlocal,voeuxactif2,numerovoeux)
    voeuxactif = reponses_candidat(ncandidats,nvoeux,voeuxactif,proposition)
    voeuxactif2 = reponses_candidat2(ncandidats,nvoeux,voeuxactif2,proposition)    
    # nous comptons le nombre de candidats n'ayant aucun "oui" pour les deux approches
    pasdeplace = 0
    pasdeplace2 = 0    
    for candidat in range(0,ncandidats):
        if (sum(proposition[candidat]) == 0):
            pasdeplace = pasdeplace + 1
        if (sum(proposition2[candidat]) == 0):
            pasdeplace2 = pasdeplace2 + 1            
    nrefus[tours] = pasdeplace
    nrefus2[tours] = pasdeplace2
# nous calculons la fraction de candidats ayant aucun "oui"
dodo = np.float32(nrefus)/np.float32(ncandidats)
dodo2 = np.float32(nrefus2)/np.float32(ncandidats)
         
print(dodo)
print(dodo[0:-1]/dodo[1::])
print(dodo2)
print(dodo2[0:-1]/dodo2[1::])

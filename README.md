Very simple simulation of the behaviour of the parcoursup procedure

pss5.py
=======

pss5.py is a full python script which is self-contained, but it uses
numpy
and
numba

In as much as numpy is essential, numba is a "just in time" python compiler that
accelerates significantly the simulation when the number of candidates and/or formations
is large. It can sometimes be difficult to install numba.  If one wants to use the script 
without numba, one can eliminate the line

from numba import jit

and one has then to remove all the decorators:
@jit

One will then run the script in native python.  This is much slower for large samples, but the
results are identical.

The script is self-contained in that it includes all the specific functions.  These functions set up
a randomly generated set of candidates, wish lists and institutions, as well as the ordered lists of
candidates by the institutions, and the number of places available in each institution.
Then there are functions corresponding to the two alternating phases of the system:  one function looks
from the institution side and uses the ordered student list of an institution to assign places 
to the students that are still interested ;
the other function looks from the student side and picks the most interesting proposition of the moment.
There are two possible responses.  In parcoursup, one is obliged to pick only one (the most interesting)
proposition and to discard all others ; however, one could still maintain all wishes that have not yet 
been satisfied, or one could also discard those wishes that are not interesting any more, given the
already obtained offer.  Maintaining wishes in which one is not interested any more is technically
possible in parcoursup, doesn't have any good reason, and will influence the convergence.

pss6.py
=======

Same as pss5.py, except that we run over different values of diversity, and we make plots of the
convergence curves in "results".

pss7.py
=======

Same as pss5.py, except that we plot the "wish satisfaction", that is, the fraction of candidates
that got their first wish, their second wish, their third wish, and so on, satisfied.

Directory "test"
================

In the directory "test", there are initial trials of the code, bits and pieces of test code and so on.
There's nothing useful there.

Directory "results"
===================

Results of the simulation.
